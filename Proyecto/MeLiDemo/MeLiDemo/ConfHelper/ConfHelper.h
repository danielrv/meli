//
//  ConfHelper.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//


/*  Clase Helper para Obtener Configuracíon de Conexión
 */
#import <Foundation/Foundation.h>
/*!
 * @discussion Clase para obtener RUTAS de servicios desde AppConfig.plist de forma facil.
 */
@interface ConfHelper : NSObject

@property (nonatomic,strong) NSString *MPBaseUrl;
@property (nonatomic,strong) NSString *clientKey;

/*  defaultMercadoPagoConfig obtiene un singleton, lee el archivo AppConfig.plist para obtener
*   La URL Base de los servicios y la llave de Cliente
 */
/*!
 @discussion Metodo para obtener instancia con valores por defecto, leyendo AppConfig.plist
 @return Instancia ConfHelper con valores por defecto
 */
+ (ConfHelper*)defaultMercadoPagoConfig;
/*!
 @discussion Metodo para obtener Ruta de servicio con un servicio especifico y parametros

 @param service NSString con el nombre del servicio solictado
 @param parametros NSDictionary Diccionario con los parametros a pasar en la URL
 @return NSURL con la URL para consumir
 */
+ (NSURL*)urlParaServicio:(NSString*)service ConParametros:(NSDictionary*)parametros;
/*!
 @discussion Metodo para obtener la Ruta de un servicio con un metodo especifico y paramtros

 @param service NSString nombre del servicio
 @param metodo NSString nombre del metodo que se deasea invocar
 @param parametros NSDictionary Diccionario con los parametros necesarios
 @return NSURL con la URL para consumir
 @code [ConfHelper urlParaServicio:@"servicio" conMetodo:@"metodo" ConParametros:@{@"llave":@"valor"}];
 */
+ (NSURL*)urlParaServicio:(NSString*)service conMetodo:(NSString*)metodo ConParametros:(NSDictionary*)parametros;
@end
