//
//  ConfHelper.m
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import "ConfHelper.h"

@implementation ConfHelper



- (id)init {
    if (self = [super init]) {
    }
    return self;
}


+ (ConfHelper*)defaultMercadoPagoConfig{
    
    
    //defaultMercadoPagoConfig Devuelve Singleton
    static ConfHelper *conf = nil;
    static dispatch_once_t onlyOnce;
    dispatch_once(&onlyOnce,^{
       conf = [[self alloc] init];
    });
    
    //Obtenemos Datos de AppConfig.plist
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"AppConfig" ofType:@"plist"]];

    //Reviamos que El archivo y las llaves esten OK, Log de NO ser así
    if(!dictionary){
        [EepLog ErrorLog:@"PLIST AppConfig NO ha sido Cargado!"];
        //quizás abort es algo brusco...
        //abort();
    }else{
        [EepLog InfoLog:@"PLIST Cargado: %@",dictionary];
        
        conf.MPBaseUrl = [dictionary valueForKey:@"MPBaseURL"];
        conf.clientKey = [dictionary valueForKey:@"MPClientKey"];
        
        if (![conf MPBaseUrl]) {
            [EepLog ErrorLog:@"Archivo de configuración NO ha encontrado llave MPBaseURL"];
            //abort();
        }
        if(![conf clientKey]){
            [EepLog ErrorLog:@"Archvio de Configuración NO ha encontrado llave clientKey"];
            //abort();
        }
    }
    return conf;
    
}

+ (NSURL*)urlParaServicio:(NSString*)service ConParametros:(NSDictionary*)parametros{
    
    NSMutableString *base = [NSMutableString new];
    [base appendString:[ConfHelper defaultMercadoPagoConfig].MPBaseUrl];
    [base appendString:service];
    [base appendString:[NSString stringWithFormat:@"?public_key=%@",[ConfHelper defaultMercadoPagoConfig].clientKey]];
    if(parametros){
        for(NSString *key in parametros){
            NSString *value = [parametros valueForKey:key];
            [base appendString:[NSString stringWithFormat:@"&%@=%@",key,value]];
        }
    }
    
    [EepLog InfoLog:@"%s -> %@",__PRETTY_FUNCTION__,base];
    NSURL *url = [NSURL URLWithString:[base stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
                                       //stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    return url;
    
}

+ (NSURL*)urlParaServicio:(NSString*)service conMetodo:(NSString*)metodo ConParametros:(NSDictionary*)parametros{
    

    NSMutableString *base = [NSMutableString new];
    [base appendString:[ConfHelper defaultMercadoPagoConfig].MPBaseUrl];
    [base appendString:service];
    [base appendString:[NSString stringWithFormat:@"/%@",metodo]];
    [base appendString:[NSString stringWithFormat:@"?public_key=%@",[ConfHelper defaultMercadoPagoConfig].clientKey]];
    if(parametros){
        for(NSString *key in parametros){
            NSString *value = [parametros valueForKey:key];
            [base appendString:[NSString stringWithFormat:@"&%@=%@",key,value]];
        }
    }
    
    [EepLog InfoLog:@"%s -> %@",__PRETTY_FUNCTION__,base];
    NSURL *url = [NSURL URLWithString:[base stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    //stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    return url;
}
@end
