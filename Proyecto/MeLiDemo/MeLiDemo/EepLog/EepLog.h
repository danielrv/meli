//
//  EepLog.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

/*  EepLog es una pequeña Clase para Logear distintos tipos mensajes en Consola.
    InfoLog:(NSString*)message,... Muestra mensajes de Información relevante o solo debug
    WarnLog:(NSString*)message,... Muestra mensajes de advertencia, pero no criticos
    ErrorLog:(NSString*)message,...Muestra mensajes de Error Criticos para el correcto funcionamiento
    FutureLog:(NSString*)message,... Para mostrar mensajes o volares esperados
 
    Para cada tipo se debe Definir
 
    #define ShowInfoLog     <-Activa InfoLog
    #define ShowWarnLog     <-Activa WarnLog
    #define ShowErrorLog    <-Activa ErrorLog
    #define ShowFutureLog   <-Activa FutureLog
 
    Eep!
 */
#import <Foundation/Foundation.h>

/*!
 @discussion EepLog es una pequeña Clase para Logear distintos tipos mensajes en Consola.
 */
@interface EepLog : NSObject


/*!
 @discussion InfoLog permite escribir en consola mensajes de informacion
 @param message NSString el mensaje que se desea escribir, junto a las variables
 @code [EepLog InfoLog:@"%@",@"Hola"];
 */
+(void)InfoLog:(NSString*)message,...;
/*!
 @discussion WarnLog permite escribir en consola mensajes de Advertencia
 @param message NSString el mensaje que se desea escribir, junto a las variables
 @code [EepLog WarnLog:@"%@",@"advertencia"];
 */
+(void)WarnLog:(NSString*)message,...;
/*!
 @discussion ErrorLog permite escribir en consola mensajes de Error
 @param message NSString el mensaje que se desea escribir, junto a las variables
 @code [EepLog ErrorLog:@"%@",@"Errror"];
 */
+(void)ErrorLog:(NSString*)message,...;
/*!
 @discussion FutureLog permite escribir en consola mensajes de informacion esperada
 @param message NSString el mensaje que se desea escribir, junto a las variables
 @code [EepLog FutureLog:@"%@",@"Hola"];
 */
+(void)FutureLog:(NSString*)message,...;
/*!
 @discussion HIDLog permite escribir en consola mensajes acerca de interaccion del usuario
 @param message NSString el mensaje que se desea escribir, junto a las variables
 @code [EepLog HIDLog:@"%@",@"Hola"];
 */
+(void)HIDLog:(NSString*)message,...;
@end
