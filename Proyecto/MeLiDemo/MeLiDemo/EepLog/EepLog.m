//
//  EepLog.m
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import "EepLog.h"

@implementation EepLog

+(void)InfoLog:(NSString*)message,...{
    
 
        va_list ap;
        va_start(ap, message);
        if (![message hasSuffix:@"\n"]) {
            message = [message stringByAppendingString:@"\n"];
        }
        NSString *log = [[NSString alloc] initWithFormat:message arguments:ap];
#pragma unused(log)
        va_end(ap);
#ifdef ShowInfoLog
        NSLog(@"✏️ %@",log);
#endif

    
    
}

+(void)WarnLog:(NSString*)message,...{
    va_list ap;
    va_start(ap, message);
    if (![message hasSuffix:@"\n"]) {
        message = [message stringByAppendingString:@"\n"];
    }
    NSString *log = [[NSString alloc] initWithFormat:message arguments:ap];
    #pragma unused(log)
    va_end(ap);

#ifdef ShowWarnLog
    NSLog(@"⚠️ %@",log);
#endif
}

+(void)ErrorLog:(NSString*)message,...{
    va_list ap;
    va_start(ap, message);
    if (![message hasSuffix:@"\n"]) {
        message = [message stringByAppendingString:@"\n"];
    }
    NSString *log = [[NSString alloc] initWithFormat:message arguments:ap];
    #pragma unused(log)
    va_end(ap);
#ifdef ShowErrorLog
    NSLog(@"❌ %@",log);
#endif
}

+(void)FutureLog:(NSString*)message,...{
    va_list ap;
    va_start(ap, message);
    if (![message hasSuffix:@"\n"]) {
        message = [message stringByAppendingString:@"\n"];
    }
    NSString *log = [[NSString alloc] initWithFormat:message arguments:ap];
    #pragma unused(log)
    va_end(ap);
#ifdef ShowFutureLog
    NSLog(@"➡️ %@",log);
#endif
    
}

+(void)HIDLog:(NSString*)message,...{
    va_list ap;
    va_start(ap, message);
    if (![message hasSuffix:@"\n"]) {
        message = [message stringByAppendingString:@"\n"];
    }
    NSString *log = [[NSString alloc] initWithFormat:message arguments:ap];
#pragma unused(log)
    va_end(ap);
#ifdef ShowFutureLog
    NSLog(@"👆 %@",log);
#endif
    
}
@end
