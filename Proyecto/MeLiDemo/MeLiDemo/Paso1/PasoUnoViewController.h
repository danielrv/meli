//
//  PasoUnoViewController.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+Alertas.h"
#import "Transaccion.h"
/**
 Clase para El primer Paso del flujo, donde se ingresa el total del monto como numero
 */
@interface PasoUnoViewController : UIViewController<UITextFieldDelegate>

/**
 Funcion para hacer unwind desde PasoCuatroViewController a PasoUnoViewController

 @param segue referencia al segue segue
 */
- (IBAction)unwindAPasoUno:(UIStoryboardSegue*)segue;
/**
 funcion para finalizar una transaccion, mostrar los resultados en pantalla

 @param transaccion la transaccion finalizada
 */
- (void)finalizarProcesoYEmpezarOtro:(Transaccion*)transaccion;
@end
