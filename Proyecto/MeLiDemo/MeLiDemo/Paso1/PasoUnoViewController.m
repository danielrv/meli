//
//  PasoUnoViewController.m
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import "PasoUnoViewController.h"
#import "PasoDosViewController.h"

@interface PasoUnoViewController ()

@property (weak, nonatomic) IBOutlet UITextField *txtIngresoMonto;
@property (nonatomic) NSNumber *montoIngresado;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *vistaInfofinal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vistaInfoFinalTopC;

@property (weak, nonatomic) IBOutlet UILabel *lblMonto;

@property (weak, nonatomic) IBOutlet UILabel *lblMedioPago;

@property (weak, nonatomic) IBOutlet UILabel *lblInstFinanciera;

@property (weak, nonatomic) IBOutlet UILabel *lblNumCuota;

@property (nonatomic) NSNumberFormatter *currcFormatter;

@property (nonatomic) NSMutableString *inpStrMonto;
@end

@implementation PasoUnoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_vistaInfofinal setHidden:YES];
    [_vistaInfoFinalTopC setConstant:-500];
    [self pretty];
    _inpStrMonto = [[NSMutableString alloc] init];
    _currcFormatter = [[NSNumberFormatter alloc] init];
    [_currcFormatter setNumberStyle:NSNumberFormatterCurrencyAccountingStyle];
    [_currcFormatter setMaximumFractionDigits:0];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    [EepLog InfoLog:@"%s",__PRETTY_FUNCTION__];
    
    Transaccion *transaccionInicial = [Transaccion new];
    [transaccionInicial setMontoTransaccion:_montoIngresado];
    
    PasoDosViewController *pdvc = (PasoDosViewController*)[segue destinationViewController];
    [pdvc setTransaccionActual:transaccionInicial];
    
    
    return;
}
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    return [self esMontoAceptable:_txtIngresoMonto];
}
#pragma mark - Monto TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [EepLog InfoLog:@"%s",__PRETTY_FUNCTION__];
    [textField resignFirstResponder];
    [self shouldPerformSegueWithIdentifier:@"PasoADos" sender:self];
    return YES;
}

/**
 Funcion para revisar si el monto ingresado por el usuario cumple con algunos requesitos minimos

 @param textField referencia al TextField de donde se debe sacar dicha informacion
 @return BOOL NO si NO cumple con los requesitos minimos
 */
- (BOOL)esMontoAceptable:(UITextField*)textField{
    
    //Limpiar espacios en blanco y saltos de linea
    NSString *enterValue = [[textField text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *mensaje = nil;
    //revisar que enterValue NO es Nil
    if (!enterValue) {
        [EepLog WarnLog:@"enterValue es Nil"];
        return NO;
    }
    //Revisar que largo sea mayor que 0
    if ([enterValue length] == 0) {
        [EepLog WarnLog:@"NO se ha ingresado monto"];
        mensaje = @"Debes Ingresar un Monto";
        [self mostrarAlertConTitulo:@"¡Ops!" mensaje:mensaje bloqueFinal:nil];
        return NO;
    }
    
    //Revisar que solo sea numeros
    NSCharacterSet *letras = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([enterValue rangeOfCharacterFromSet:letras].location != NSNotFound) {
        [EepLog WarnLog:@"Hay Letras"];
        mensaje = @"Deben ser solo numeros";
        [self mostrarAlertConTitulo:@"¡Ops!" mensaje:mensaje bloqueFinal:nil];
        return NO;
    }
    
    //Ya que son numeros, lo volvemos Numero. Entero?
    _montoIngresado = [NSNumber numberWithInt:[enterValue intValue]];
    if (!_montoIngresado){
        [EepLog WarnLog:@"No se ha podido transformar enterValue a NSNumber"];
        mensaje = @"No se ha podido transformar el numero";
        [self mostrarAlertConTitulo:@"¡Ops!" mensaje:mensaje bloqueFinal:nil];
        return NO;
    }
    [EepLog InfoLog:@"Monto Ingrsado es: %d",[_montoIngresado intValue]];
    
    //Finalmente revisamos que no sea 0
    if ([_montoIngresado intValue] == 0) {
        [EepLog WarnLog:@"Monto Ingresado es 0"];
        mensaje = @"El Monto debe ser superior a 0";
        [self mostrarAlertConTitulo:@"¡Ops!" mensaje:mensaje bloqueFinal:nil];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)unwindAPasoUno:(UIStoryboardSegue*)segue{
    
    
}
/**
 Funcion para mostrar informacion de una transaccion en pantalla

 @param transaccion instancia de la transaccion que se desea mostrar
 */
- (void)finalizarProcesoYEmpezarOtro:(Transaccion*)transaccion{
    //Llenar campos con la informacion de la transaccion
    [_lblMedioPago setText:transaccion.medioPago.name];
    [_lblMonto setText:[_currcFormatter stringFromNumber:[transaccion montoTransaccion]]]; //[[transaccion montoTransaccion] stringValue]];
    [_lblInstFinanciera setText:transaccion.financiera.name];
    [_lblNumCuota setText:transaccion.cuota.recommended_message];
    
    
    
    [_txtIngresoMonto setText:@""];
    [_vistaInfofinal setHidden:NO];
    [_vistaInfoFinalTopC setConstant:0];
    
    //presentar vista con animacion
    [UIView animateWithDuration:0.8 animations:^{
        [self.view layoutIfNeeded];
    }];
    transaccion = nil;
   
}

/**
 Funcion para ocultar informacion de transaccion finzalizada

 @param sender referencia al objecto que la invoca
 */
- (IBAction)cerrarInfoFinal:(id)sender {

    [_vistaInfoFinalTopC setConstant:-500];
    [UIView animateWithDuration:0.9 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (finished) {
            [_vistaInfofinal setHidden:YES];
        }
    }];
}

@end
