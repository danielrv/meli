//
//  PasoDosViewController.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"
#import "ServiceHelper.h"
#import "Transaccion.h"
#import "UIViewController+Alertas.h"
#import "PasoTresViewController.h"

/**
 Clase para El segundo Paso del flujo, donde se elige un Medio de Pago
 */
@interface PasoDosViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic)Transaccion *transaccionActual;

@end
