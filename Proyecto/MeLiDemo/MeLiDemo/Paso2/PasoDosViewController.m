//
//  PasoDosViewController.m
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import "PasoDosViewController.h"
#import "ConfHelper.h"
#import "LoadingTableViewCell.h"
#import "MetodoPagoTableViewCell.h"
#import "MedioPago.h"
#import "ImageHelper.h"
@interface PasoDosViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tabla;
/**
 NSArraly, lista local para guardar medios de pago
 */
@property (nonatomic) NSArray *listaMetodosDePago;
/**
 Diccionario para guardar progresos de imagenes
 */
@property (nonatomic, strong) NSMutableDictionary *descargaImagenesEnProgreso;



@end

@implementation PasoDosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self pretty];
    _listaMetodosDePago = [NSArray array];
    [EepLog InfoLog:@"Inicio Paso Dos con Transaccion Monto: %d",[_transaccionActual.montoTransaccion intValue]];

    //obtenemos la url del servicio y llamaos en otro hilo
    NSURL *urlMetodosPago = [ConfHelper urlParaServicio:@"payment_methods" ConParametros:nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self iniciarDescargaDeDatosParaURL:urlMetodosPago];
    });
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [EepLog WarnLog:@"Queda poca memoría, olvidar imagenes"];
    [self cancelarTodasLasDescargas];
}

- (void)iniciarDescargaDeDatosParaURL:(NSURL*)url{
    [EepLog InfoLog:@"%s %@",__PRETTY_FUNCTION__,url];
    BOOL intenert = [[Reachability reachabilityWithHostName:@"api.mercadopago.com"] currentReachabilityStatus];
    if (intenert == NotReachable) {
        [EepLog WarnLog:@"No Hay Conexión"];
        [self mostrarAlertConTitulo:@"Ops!" mensaje:@"No Hay Conexión" bloqueFinal:^{
            //[self.navigationController popViewControllerAnimated:YES];
        }];
        return;
        
    }
    
    ServiceHelper *service = [[ServiceHelper alloc] init];
    [service obtenerArrayDe:MedioPago.class ConUrl:url completado:^(NSArray *lista) {
        
        if(lista){
            //si obtenemos lista
            //actualizamos tabla
            dispatch_async(dispatch_get_main_queue(), ^{
                _listaMetodosDePago = lista;
                [_tabla reloadData];
            });
        }else{
            //De lo contrario ha ocurrido un error...
            [self mostrarAlertConTitulo:@"Ops" mensaje:@"Ha ocurrido un error al obtener Medios de Pago" bloqueFinal:^{
                
            }];
        }
        
    }];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    [EepLog InfoLog:@"%s",__PRETTY_FUNCTION__];
}


#pragma mark - TablaDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
    //Si la lista es 0 entonces, colocar un row para el loader
    return [_listaMetodosDePago count]>0 ? [_listaMetodosDePago count]: 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_listaMetodosDePago.count == 0) {
        LoadingTableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"LoadingTableViewCell" forIndexPath:indexPath];
        return celda;
        
    }
    MedioPago *mp = [_listaMetodosDePago objectAtIndex:indexPath.row];
    MetodoPagoTableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"MetodoPagoTableViewCell"];
    [celda.lblName setText:[mp name]];
    
    if(!mp.imagenThumbnail){
        [self empezarDescargaPara:mp paraIndexPath:indexPath];
    }else{
        [celda.imgThumbnail setImage:mp.imagenThumbnail];
    }
    return celda;
 
    
}
/**
 funcion para llamar a ImageHelper y obtener imagenes para varios MedioPago

 @param medioPago el objecto MedioPago que debe descargar su imagen
 @param indexPath NSIndexPath del medio de pago en la tabla
 */
- (void)empezarDescargaPara:(MedioPago*)medioPago paraIndexPath:(NSIndexPath*)indexPath{
    
    NSLog(@"%s",__PRETTY_FUNCTION__);
    ImageHelper *imgHelper = (self.descargaImagenesEnProgreso)[indexPath];
    if(!imgHelper){
        imgHelper = [[ImageHelper alloc] init];
        [imgHelper setBloqueCompleto:^{
            MetodoPagoTableViewCell *celda = (MetodoPagoTableViewCell*)[_tabla cellForRowAtIndexPath:indexPath];
            [celda.imgThumbnail setImage:medioPago.imagenThumbnail];
        }];
        
        (self.descargaImagenesEnProgreso)[indexPath] = imgHelper;
        [imgHelper empezarDescargaMedioPagoPara:medioPago];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MedioPago *medioPago = [_listaMetodosDePago objectAtIndex:indexPath.row];
    
    int monto = [[_transaccionActual montoTransaccion] intValue];
    int max = [[medioPago max_allowed_amount] intValue];
    if(monto > max){
        [EepLog WarnLog:@"Monto supera máximo!"];
        NSString *mensaje = [NSString stringWithFormat:@"Este Medio de Pago solo soporta hasta %d",max];
        [self mostrarAlertConTitulo:@"Ops" mensaje:mensaje bloqueFinal:^{
            
        }];
        return;
    }
    
    [_transaccionActual setMedioPago:medioPago];
    [EepLog HIDLog:@"%s %@",__PRETTY_FUNCTION__,medioPago.name];
    PasoTresViewController *ptvc = [self.storyboard instantiateViewControllerWithIdentifier:@"PasoTresViewController"];
    [ptvc setTransaccionActual:_transaccionActual];
    [self.navigationController pushViewController:ptvc animated:YES];
    
    
}
/**
 funcion para cancelar todas las descargas
 */
- (void)cancelarTodasLasDescargas{
    NSArray *todas = [self.descargaImagenesEnProgreso allValues];
    [todas makeObjectsPerformSelector:@selector(cancelarDescarga)];
    
    [self.descargaImagenesEnProgreso removeAllObjects];
}
@end
