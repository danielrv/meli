//
//  FinancieraTableViewCell.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinancieraTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *imgFinanciera;
@property (nonatomic,weak) IBOutlet UILabel *lblName;

@end
