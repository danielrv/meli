//
//  PasoTresViewController.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"
#import "UIViewController+Alertas.h"
#import "ServiceHelper.h"
#import "Transaccion.h"
#import "ConfHelper.h"
#import "Financiera.h"
#import "ImageHelper.h"
#import "PasoCuatroViewController.h"
/**
 Clase para El Tercer Paso del flujo, donde se elige una Ins. Financiera
 */
@interface PasoTresViewController : UIViewController

@property (nonatomic) Transaccion* transaccionActual;
@end
