//
//  PasoTresViewController.m
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import "PasoTresViewController.h"
#import "LoadingTableViewCell.h"
#import "FinancieraTableViewCell.h"
@interface PasoTresViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tabla;
/**
 Lista local para guardar objectos Financiera
 */
@property (nonatomic) NSArray *listaFinancieras;
/**
 Diccionario para guardar progresos de imagenes
 */
@property (nonatomic, strong) NSMutableDictionary *descargaImagenesEnProgreso;

@end

@implementation PasoTresViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self pretty];
    [EepLog InfoLog:@"%@",_transaccionActual.medioPago.name];
    NSURL *url = [ConfHelper urlParaServicio:@"payment_methods" conMetodo:@"card_issuers" ConParametros:@{@"payment_method_id":_transaccionActual.medioPago.idMP}];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self iniciarDescargaDeDatosParaURL:url];
    });
}

- (void)iniciarDescargaDeDatosParaURL:(NSURL*)url{
    [EepLog InfoLog:@"%s %@",__PRETTY_FUNCTION__,url];
    BOOL intenert = [[Reachability reachabilityWithHostName:@"api.mercadopago.com"] currentReachabilityStatus];
    if (intenert == NotReachable) {
        [EepLog WarnLog:@"No Hay Conexión"];
        [self mostrarAlertConTitulo:@"Ops" mensaje:@"No Hay Conexión" bloqueFinal:^{
            //[self.navigationController popViewControllerAnimated:YES];
        }];
        return;
    }
    
    ServiceHelper *service = [[ServiceHelper alloc] init];
    [service obtenerArrayDe:Financiera.class ConUrl:url completado:^(NSArray *lista) {
        if(lista){
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([lista count]>0) {
                    _listaFinancieras = lista;
                    [_tabla reloadData];
                }else{
                    [self mostrarAlertConTitulo:@"Ops!" mensaje:@"No hay bancos para este tipo, Por favor seleccione otro" bloqueFinal:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
                
            });
        }else{
            [self mostrarAlertConTitulo:@"Ops" mensaje:@"Ha ocurrido un error al obtener Medios de Pago" bloqueFinal:^{
                
            }];
        }
    }];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [EepLog WarnLog:@"Queda poca memoría, olvidar imagenes"];
    [self cancelarTodasLasDescargas];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - TablaDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //Si la lista es 0 entonces, colocar un row para el loader
    return [_listaFinancieras count]>0 ? [_listaFinancieras count]: 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_listaFinancieras.count == 0) {
        LoadingTableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"LoadingTableViewCell" forIndexPath:indexPath];
        return celda;
        
    }
    
    Financiera *financiera = [_listaFinancieras objectAtIndex:indexPath.row];
    FinancieraTableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"FinancieraTableViewCell"];
    [celda.lblName setText:[financiera name]];
    
    
    if(!financiera.imagenThumbnail){
        [self empezarDescargaPara:financiera paraIndexPath:indexPath];
    }else{
        [celda.imgFinanciera setImage:financiera.imagenThumbnail];
    }
    
    return celda;

    
}
/**
 funcion para llamar a ImageHelper y obtener imagenes para varios Financiera
 
 @param financiera el objecto Finaciera que debe descargar su imagen
 @param indexPath NSIndexPath del medio de pago en la tabla
 */
- (void)empezarDescargaPara:(Financiera*)financiera paraIndexPath:(NSIndexPath*)indexPath{
    
    NSLog(@"%s",__PRETTY_FUNCTION__);
    ImageHelper *imgHelper = (self.descargaImagenesEnProgreso)[indexPath];
    if(!imgHelper){
        imgHelper = [[ImageHelper alloc] init];
        [imgHelper setBloqueCompleto:^{
            FinancieraTableViewCell *celda = (FinancieraTableViewCell*)[_tabla cellForRowAtIndexPath:indexPath];
            [celda.imgFinanciera setImage:financiera.imagenThumbnail];
        }];
        
        (self.descargaImagenesEnProgreso)[indexPath] = imgHelper;
        [imgHelper empezarDescargaMedioPagoPara:financiera];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    Financiera *financiera = [_listaFinancieras objectAtIndex:indexPath.row];
    [EepLog HIDLog:@"%s %@",__PRETTY_FUNCTION__,financiera.name];
    [_transaccionActual setFinanciera:financiera];
    
    PasoCuatroViewController *pcvc = [self.storyboard instantiateViewControllerWithIdentifier:@"PasoCuatroViewController"];
    [pcvc setTransaccionActual:_transaccionActual];
    [self.navigationController pushViewController:pcvc animated:YES];
    
}
- (void)cancelarTodasLasDescargas{
    NSArray *todas = [self.descargaImagenesEnProgreso allValues];
    [todas makeObjectsPerformSelector:@selector(cancelarDescarga)];
    
    [self.descargaImagenesEnProgreso removeAllObjects];
}
@end
