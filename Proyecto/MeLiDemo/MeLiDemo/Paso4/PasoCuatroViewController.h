//
//  PasoCuatroViewController.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"
#import "UIViewController+Alertas.h"
#import "Transaccion.h"
#import "Cuota.h"
#import "ServiceHelper.h"
#import "ConfHelper.h"

@interface PasoCuatroViewController : UIViewController <UIPickerViewDelegate>
/**
 Clase para El cuarto Paso del flujo, donde se elige cantidad de cuotas
 */
@property (nonatomic) Transaccion *transaccionActual;

@end
