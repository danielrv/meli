//
//  PasoCuatroViewController.m
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import "PasoCuatroViewController.h"
#import "PasoUnoViewController.h"
@interface PasoCuatroViewController ()

/**
 NSArray listal local para guardar posibles cuotasd
 */
@property (nonatomic,strong)NSArray *listaCuotasDisponibles;
@property (weak, nonatomic) IBOutlet UIPickerView *cuotasPicker;

@property (weak, nonatomic) IBOutlet UIButton *btnFinalizar;


@end


@implementation PasoCuatroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pretty];
    _listaCuotasDisponibles = [NSArray array];
    // Do any additional setup after loading the view.
    NSURL *url = [ConfHelper urlParaServicio:@"payment_methods" conMetodo:@"installments" ConParametros:@{@"payment_method_id":_transaccionActual.medioPago.idMP,@"issuer.id":_transaccionActual.financiera.idFn,@"amount":[_transaccionActual montoTransaccion]}];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self iniciarDescargaDeDatosParaURL:url];
    });
    
    
}
- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
}
- (void)iniciarDescargaDeDatosParaURL:(NSURL*)url{
    [EepLog FutureLog:@"%s %@",__PRETTY_FUNCTION__,url];
    BOOL intenert = [[Reachability reachabilityWithHostName:@"api.mercadopago.com"] currentReachabilityStatus];
    if (intenert == NotReachable) {
        [EepLog WarnLog:@"No Hay Conexión"];
        [self mostrarAlertConTitulo:@"Ops" mensaje:@"No Hay Conexión" bloqueFinal:^{
            //[self.navigationController popViewControllerAnimated:YES];
        }];
        return;
    }
    ServiceHelper *serviceHelper = [[ServiceHelper alloc] init];
    [serviceHelper obtenerArrayDe:Cuota.class ConUrl:url completado:^(NSArray *lista) {
        
        if(lista){
            if ([lista count] == 0) {
                [EepLog WarnLog:@"Lista con Cuotas esta vacía"];
                [self mostrarAlertConTitulo:@"Ops" mensaje:@"No se han encontrado opciones de cuota para este pago, pruebe seleccionado otro." bloqueFinal:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    _listaCuotasDisponibles = lista;
                    [_cuotasPicker reloadAllComponents];
                    int aSeleccionar = floor(lista.count/2);
                    [_cuotasPicker selectRow:aSeleccionar inComponent:0 animated:YES];
                    [_btnFinalizar setUserInteractionEnabled:YES];
                });
            }
        }else{
            [EepLog WarnLog:@"Error al obtener lista de Cuotas!"];
            [self mostrarAlertConTitulo:@"Ops" mensaje:@"Ha ocurrido un error al buscar lista de cuotas, por favor trate mas tarde" bloqueFinal:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        
        
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.destinationViewController isKindOfClass:[PasoUnoViewController class]]) {
        PasoUnoViewController *puvc = (PasoUnoViewController*)[segue destinationViewController];
        [puvc finalizarProcesoYEmpezarOtro:_transaccionActual];
        
    }
    
}
#pragma mark - PickerView
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
    
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return _listaCuotasDisponibles.count >0 ? _listaCuotasDisponibles.count: 1;
}
- (UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    [EepLog FutureLog:@"%s",__PRETTY_FUNCTION__];
    UILabel *vista = (UILabel*)view;
    if(!vista){
        
        vista = [[UILabel alloc] init];
        [vista setTextAlignment:NSTextAlignmentCenter];
    }
    if(_listaCuotasDisponibles.count == 0){
        [vista setText:@"Cargando..."];
    }else{
        Cuota *cuota = [_listaCuotasDisponibles objectAtIndex:row];
        [vista setText:cuota.recommended_message];
    }
    
    return vista;
}


- (IBAction)finalizar:(id)sender {
    [EepLog HIDLog:@"Finalizar"];
    [EepLog HIDLog:@"%d",[_cuotasPicker selectedRowInComponent:0]];
    Cuota *cuota = [_listaCuotasDisponibles objectAtIndex:[_cuotasPicker selectedRowInComponent:0]];
    [_transaccionActual setCuota:cuota];
    [EepLog InfoLog:@"%@",cuota.recommended_message];
    
    [self performSegueWithIdentifier:@"UnwindAPasoUno" sender:self];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [EepLog HIDLog:@"%s",__PRETTY_FUNCTION__];
}
@end
