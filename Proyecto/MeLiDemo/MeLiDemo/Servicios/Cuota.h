//
//  Cuota.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
Clase para guardar información de la cuota, por ahora solo guarda el "recommended_message",
 pero debería guardar informacion relevante como num de cuotas, valor de cuota, interes, etc...
*/
@interface Cuota : NSObject

/**
 NSString donde se guarda el mensaje con la cantidad de cuotas recomendado a mostrar al usuario, proviene del servicio
 */
@property (nonnull,nonatomic,strong) NSString *recommended_message;

@end
