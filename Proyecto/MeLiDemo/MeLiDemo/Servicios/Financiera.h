//
//  Financiera.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
Clase para guardar informacion referente a la inst. financiera para pagar seleccionada por el usuario
*/
@interface Financiera : NSObject

/**
 NSString con el nombre de la ins. financiera
 */
@property (nonatomic,strong) NSString *name;
/**
 NSString con la url de la imagen a mostrar
 */
@property (nonatomic,strong) NSString *thumbnail;

/**
 NSString con el id de la inst. financiera
 */
@property (nonatomic,strong) NSString *idFn;
/**
UIImage con la imagen cargada a mostrar para esta inst. financiera
*/
@property (nonatomic, strong) UIImage *imagenThumbnail;

@end
