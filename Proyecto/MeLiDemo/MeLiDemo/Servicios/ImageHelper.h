//
//  ImageHelper.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @discussion ImageHelper es una pequeña clase para obtener imagenes.
 */
@interface ImageHelper : NSObject

/*!
 Bloque que se debe llamar cuando la descarga finalize
 */
@property (nonatomic,copy) void (^bloqueCompleto)(void);
/*!
@discussion funcion para empezar la descarga de una imagen para un objecto

@param objecto Objecto que contenga una URL valida y referencia a UIImage para guardar
 */
- (void)empezarDescargaMedioPagoPara:(id)objecto;
/*!
 @discussion funcion para cancelar la descarga actual
 */
- (void)cancelarDescarga;
@end
