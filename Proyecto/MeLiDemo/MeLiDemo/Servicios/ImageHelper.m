//
//  ImageHelper.m
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import "ImageHelper.h"
#import "MedioPago.h"
#import "Financiera.h"
@interface ImageHelper()

/*!
@discussion propiedad NSURLSessionDataTask referencia para la sesion de descarga del objecto
*/
@property (nonatomic,strong) NSURLSessionDataTask *sessionTask;

@end

@implementation ImageHelper

- (void)empezarDescargaMedioPagoPara:(id )objecto{
    [EepLog InfoLog:@"%s",__PRETTY_FUNCTION__];
    

    @try{
        //Si el objecto es de tipo MedioPago
        if ([objecto isKindOfClass:MedioPago.class]) {
            NSURLRequest *rquest = [NSURLRequest requestWithURL:[NSURL URLWithString:[(MedioPago*)objecto thumbnail]]];
            //iniciamos sessionTask
            _sessionTask = [[NSURLSession sharedSession] dataTaskWithRequest:rquest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (error) {
                    //si hay error mostrar error y setear imagen a placeholder
                    [EepLog ErrorLog:@"Error al Descargar Imagen"];
                    
                    [(MedioPago*)objecto setImagenThumbnail:[UIImage imageNamed:@"placeholder"]];
                    
                }else{
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                    if(httpResponse.statusCode !=200){
                        [(MedioPago*)objecto setImagenThumbnail:[UIImage imageNamed:@"placeholder"]];
                        return;
                    }
                    //si NO hay herror
                    [EepLog InfoLog:@"Eeep"];
                    NSString *e = [(MedioPago*)objecto thumbnail];
                    
                    //Iniciamos una cola con bloque
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        
                        //Obtenemos el primer Byte de la Data
                        NSData *byte = [data subdataWithRange:NSMakeRange(0, 1)];
                        Byte *byteData = (Byte*)malloc(1);
                        memcpy(byteData,[byte bytes],1);
                        
                        [EepLog InfoLog:@"Primer Byte de imagen %X para %@",byteData[0],e];
                        
                        //Creamos imagen con Data
                        UIImage *image = [[UIImage alloc] initWithData:data];
                        //Si el primer byte es dsitinto a 0x47, no es el tipo de imagen que queremos, o no es imagen.
                        if (byteData[0]==0x3C) {
                            image = [UIImage imageNamed:@"placeholder"];
                            [EepLog WarnLog:@"Econtro Data, pero NO es imagen"];
                        }
                        //se setea la imagen del objecto a la imagen creada
                        [(MedioPago*)objecto setImagenThumbnail:image]; //imagenThumbnail = image;
                        
                        //si esxiste el bloque bloqueCompleto, se invoca
                        if(self.bloqueCompleto){
                            //no aseguramos de llamarlo en el main_queue
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.bloqueCompleto();
                            });
                            
                        }
                    }];
                }
                
            }];
            //se "corre" sessionTask
            [_sessionTask resume];
        }
        //Si el objecto es de tipo Financiera
        if ([objecto isKindOfClass:Financiera.class]) {
            NSURLRequest *rquest = [NSURLRequest requestWithURL:[NSURL URLWithString:[(MedioPago*)objecto thumbnail]]];
            //iniciamos sessionTask
            _sessionTask = [[NSURLSession sharedSession] dataTaskWithRequest:rquest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (error) {
                    //si hay error mostrar error y setear imagen a placeholder
                    [EepLog ErrorLog:@"Error al Descargar Imagen"];
                    
                    [(Financiera*)objecto setImagenThumbnail:[UIImage imageNamed:@"placeholder"]];
                    
                }else{
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                    if(httpResponse.statusCode !=200){
                        [(Financiera*)objecto setImagenThumbnail:[UIImage imageNamed:@"placeholder"]];
                        return;
                    }
                    //si NO hay herror
                    [EepLog InfoLog:@"Eeep"];
                    NSString *e = [(Financiera*)objecto thumbnail];
                    //Iniciamos una cola con bloque
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        
                        //Obtenemos el primer Byte de la Data
                        NSData *byte = [data subdataWithRange:NSMakeRange(0, 1)];
                        Byte *byteData = (Byte*)malloc(1);
                        memcpy(byteData,[byte bytes],1);
                        
                        [EepLog InfoLog:@"Primer Byte de imagen %X para %@",byteData[0],e];
                        
                        //Creamos imagen con Data
                        UIImage *image = [[UIImage alloc] initWithData:data];
                        //Si el primer byte es dsitinto a 0x47, no es el tipo de imagen que queremos, o no es imagen.
                        if (byteData[0]==0x3C) {
                            image = [UIImage imageNamed:@"placeholder"];
                            [EepLog WarnLog:@"Econtro Data, pero NO es imagen"];
                        }
                        
                        [(Financiera*)objecto setImagenThumbnail:image]; //imagenThumbnail = image;
                        
                        if(self.bloqueCompleto){
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.bloqueCompleto();
                            });
                            
                        }
                    }];
                }
                
            }];
            //se "corre" sessionTask
            [_sessionTask resume];
        }
        
    }
    @catch(NSException *exception){
        [EepLog ErrorLog:@"Error! %@",exception.description];
    }
   
    
    
}
/**
 Cancela la descarga actual y termina la session
 */
-(void)cancelarDescarga{
    [self.sessionTask cancel];
    _sessionTask = nil;
}

@end
