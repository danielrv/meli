//
//  MedioPago.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
/**
 Clase para guardar informacion referente al medio de pago seleccionada por el usuario
 */
@interface MedioPago : NSObject

/**
 propiedad NSString para guardar el nombre del Medio de Pago
 */
@property (nonatomic,strong) NSString *name;
/**
 propiedad NSString para guardar la ruta al thumbnail
 */
@property (nonatomic,strong) NSString *thumbnail;
/**
 propiedad NSString para guardar el ID del medio de pago
 */
@property (nonatomic,strong) NSString *idMP;
/**
 propiedad UIImage para guardar referencias a la imgen del medio de pago
 */
@property (nonatomic, strong) UIImage *imagenThumbnail;
/**
 propiedad NSNumber para guardar el monto maximo disponible para pagar es el medio de pago
 */
@property (nonatomic,strong) NSNumber *max_allowed_amount;


@end
