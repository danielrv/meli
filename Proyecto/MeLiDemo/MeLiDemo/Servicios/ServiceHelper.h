//
//  ServiceHelper.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>
/*!
 @discussion ServiceHelper es una pequeña clase para obtener los datos de los servicios .
 */
@interface ServiceHelper : NSObject<NSURLSessionDelegate>

/*!
  @discussion funcion para obtener desde una URL los datos necesarios para cada paso del flujo

 @param tipo referencia al tipo de objecto que se espera
 @param url la url final donde debe buscar el servicio
 @param finalBlock bloque de ejecuccion final despues de obtener los datos
 */
- (void)obtenerArrayDe:(id)tipo ConUrl:(NSURL*)url completado:(void(^)(NSArray* lista))finalBlock;

@end
