//
//  ServiceHelper.m
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import "ServiceHelper.h"
#import "MedioPago.h"
#import "Financiera.h"
#import "Cuota.h"
@implementation ServiceHelper

- (void)obtenerArrayDe:(id)tipo ConUrl:(NSURL*)url completado:(void(^)(NSArray* lista))finalBlock{
    //Obtenemos un NSURLSessionConfiguration por defecto
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
    //Creamos una session, con delegado self y nueva cola de operacion
    NSURLSession *session = [NSURLSession sessionWithConfiguration:conf delegate:self delegateQueue:[NSOperationQueue new]];
    //Creamos un NSURLSessionDataTask con la URL
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if(error){
            //Si hay error entonces informar y enviar bloque final con nil
            [EepLog ErrorLog:@"Error en ServiceHelper %s dataTaskCompletionHandler",__PRETTY_FUNCTION__];
            [EepLog ErrorLog:@"%@",error.description];
            if (finalBlock) {
                finalBlock(nil);
            }
        }else{
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //revisamos status code
          
            if(httpResponse.statusCode != 200){
                if(finalBlock){
                    finalBlock(nil);
                }
                
                return;
            }
            
            //Si NO hay error
            NSError *obtenerMediosDePagoError; //Error para Serialización de JSON
            //Transformamos la respuesta a Array
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&obtenerMediosDePagoError];
            
            if(!obtenerMediosDePagoError){
                [EepLog FutureLog:@"Se obtuvo JSON"];
                @try{
                    //Yay Tenemos JSON
                    
                    //Si es tipo MedioPago
                    if ([tipo isEqual:MedioPago.class]) {
                        [EepLog InfoLog:@"Es MedioPago"];
                        //Creamos un NSMutableArray para guardar los Medio de Pago
                        NSMutableArray *listaDeMediosDePago = [[NSMutableArray alloc] init];
                        //Para cada NSDictionary en el array json
                        for (NSDictionary *dk in json) {
                            
                            
                            //seteamos los valores del array, si no existen, ponemos por defecto
                            NSString *name =  [dk valueForKey:@"name"]? [dk valueForKey:@"name"] : @"NO_NAME";
                            NSString *thumbnail =  [dk valueForKey:@"secure_thumbnail"]? [dk valueForKey:@"thumbnail"] : @"NO_THUMB";
                            NSString *idMp =  [dk valueForKey:@"id"]? [dk valueForKey:@"id"] : @"NO_ID";
                            
                            NSNumber *maxAmount = [dk valueForKeyPath:@"max_allowed_amount"]? [NSNumber numberWithInt:[[dk valueForKeyPath:@"max_allowed_amount"]intValue]] : [NSNumber numberWithInt:0];
                            
                            //si los datos estan bien, entonces  creamos y guardamos Medios de Pago
                            if(![name isEqualToString:@"NO_NAME"]){
                                //Creamos una instancia de medio de pago
                                MedioPago *medioPago = [MedioPago new];
                                [medioPago setName:name];
                                [medioPago setThumbnail:thumbnail];
                                [medioPago setIdMP:idMp];
                                [medioPago setMax_allowed_amount:maxAmount];
                                //agregamos el objecto al arreglo
                                [listaDeMediosDePago addObject:medioPago];
                            }
                            
                        }//fin for
                        //despues del for, deberiamos tener un arreglo con algunos medios de pago
                        //se invoca al finalBlock con el arreglo
                        finalBlock(listaDeMediosDePago);
                    }
                    //fin If MedioPago.class
                    if([tipo isEqual:Financiera.class]){
                        [EepLog InfoLog:@"Es Financiera"];
                        NSMutableArray *listaDeFinancieras = [[NSMutableArray alloc] init];
                        for (NSDictionary *dk in json) {
                            
                            
                            Financiera *financiera = [Financiera new];
                            NSString *name =  [dk valueForKey:@"name"]? [dk valueForKey:@"name"] : @"NO_NAME";
                            NSString *thumbnail =  [dk valueForKey:@"secure_thumbnail"]? [dk valueForKey:@"thumbnail"] : @"NO_THUMB";
                            NSString *idFn =  [dk valueForKey:@"id"]? [dk valueForKey:@"id"] : @"NO_ID";
                            
                            [financiera setName:name];
                            [financiera setThumbnail:thumbnail];
                            [financiera setIdFn:idFn];
                            
                            [listaDeFinancieras addObject:financiera];
                        }//fin for
                        finalBlock(listaDeFinancieras);
                    }
                    //Fin tipo Financiera
                    //Si es tipo Cuota
                    if ([tipo isEqual:Cuota.class]) {
                        [EepLog InfoLog:@"Es Cuota"];
                        NSMutableArray *listaDeCuotas = [[NSMutableArray alloc] init];
                        for (NSDictionary *dk in json) {
                            
                            [EepLog InfoLog:@"%@",[dk valueForKeyPath:@"payer_costs.recommended_message"]];
                            
                            
                            NSArray *recommended_messageArray =  [dk valueForKeyPath:@"payer_costs.recommended_message"]? [dk valueForKeyPath:@"payer_costs.recommended_message"] : @"NO_STR";
                            for (NSString *messageCuota in recommended_messageArray) {
                                [EepLog FutureLog:@"%@",messageCuota];
                                Cuota *cuota = [Cuota new];
                                [cuota setRecommended_message:messageCuota];
                                [listaDeCuotas addObject:cuota];
                            }
                            
                            
                        }//fin for
                        finalBlock(listaDeCuotas);
                    }
                    
                    
                    
                    
                }
                @catch(NSException *ex){
                    
                }
               
                
            }else{
                [EepLog WarnLog:@"No se obtuvo JSON"];
                if(finalBlock){
                    finalBlock(nil);
                }
            }
            
            
        }//fin else
    }];
    
    [dataTask resume];
    
}
@end
