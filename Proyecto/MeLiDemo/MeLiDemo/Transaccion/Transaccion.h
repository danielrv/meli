//
//  Transaccion.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MedioPago.h"
#import "Financiera.h"
#import "Cuota.h"

/*!
 @discussion Clase para guardar datos de la transacción actual
 */
@interface Transaccion : NSObject

/**
 Monto ingresado por el usuario guardado como NSNumber
 */
@property (nonatomic,nonnull,strong) NSNumber *montoTransaccion;
/**
 Objecto Medio de Pago elegido por el usuario
 */
@property (nonatomic,nonnull,strong) MedioPago *medioPago;
/**
 Objecto Ins. Financiera elegido por el usuario
 */
@property (nonatomic,nonnull,strong) Financiera *financiera;
/**
 Objecto Cuota elegido por el usuario
 */
@property (nonatomic,nonnull,strong) Cuota *cuota;
@end
