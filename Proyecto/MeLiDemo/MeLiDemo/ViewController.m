//
//  ViewController.m
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import "ViewController.h"
#import "ConfHelper.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imagenLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imagenTopConstraint;

@property (weak, nonatomic) IBOutlet UILabel *lblDemoText;


@property (weak, nonatomic) IBOutlet UIButton *btnEmpezar;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    // Revisamos ConfHelper
    ConfHelper *confHelper = [ConfHelper defaultMercadoPagoConfig];
    
    [EepLog FutureLog:@"Debería llegar: https://api.mercadopago.com/v1/ y 444a9ef5‐8a6b‐429f‐abdf‐587639155d88"];
    [EepLog InfoLog:@"En Efecto llego: %@ y %@",confHelper.MPBaseUrl,confHelper.clientKey];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _imagenTopConstraint.constant = 20;
    [UIView animateWithDuration:0.8 animations:^{
        _imagenLogo.transform = CGAffineTransformMakeScale(0.5, 0.5);
        [_btnEmpezar setAlpha:1];
        [_lblDemoText setAlpha:1];
        [self.view layoutIfNeeded];
        
    }];
}

@end
