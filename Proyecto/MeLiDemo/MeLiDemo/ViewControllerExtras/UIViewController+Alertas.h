//
//  UIViewController+Alertas.h
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaccion.h"

/*!
 @discussion UIViewController+Alertas es una categoria (Category) para UIViewController que permite mostrar alertas desde una funcion
 */
@interface UIViewController (Alertas)

/*!
 @discussion mostrarAlertConTitulo es una funcion para mostrar Alertas estandar desde cualquier view controller que use la categoria

 @param titulo NSString titulo de la alerta para mostrar
 @param mensaje NSString mensaje de la alerta para mostrar
 @param bloque block que se invoca al presionar el boton por defecto
 */
-(void) mostrarAlertConTitulo:(NSString*)titulo mensaje:(NSString*)mensaje bloqueFinal:(void(^)(void))bloque;
-(void) mostrarMensajeFinalConTransaccion:(Transaccion*)transaccion;

-(void)pretty;
@end
