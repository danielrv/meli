//
//  UIViewController+Alertas.m
//  MeLiDemo
//
//  Created by Daniel Rodriguez on 21-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import "UIViewController+Alertas.h"

@implementation UIViewController (Alertas)

-(void)pretty{
    UIImageView *imgTitle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"main_logo"]];
    [imgTitle setContentMode:UIViewContentModeScaleAspectFit];
    [self.navigationItem setTitleView:imgTitle];
}
-(void) mostrarAlertConTitulo:(NSString*)titulo mensaje:(NSString*)mensaje bloqueFinal:(void(^)(void))bloque{
    
    UIAlertController *alerta = [UIAlertController alertControllerWithTitle:titulo message:mensaje preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(bloque){
            bloque();
        }
    }];
    [alerta addAction:okAction];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alerta animated:YES completion:^{
            
        }];
    });
    
    
    
}
-(void) mostrarMensajeFinalConTransaccion:(Transaccion*)transaccion{
    
    UIAlertController *alerta = [UIAlertController alertControllerWithTitle:@"Finalizo" message:@"Proceso Finalizo" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alerta addAction:okAction];
    [self presentViewController:alerta animated:YES completion:nil];
}
@end
