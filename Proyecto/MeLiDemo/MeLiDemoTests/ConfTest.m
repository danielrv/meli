//
//  ConfTest.m
//  MeLiDemoTests
//
//  Created by Daniel Rodriguez on 22-12-17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ConfHelper.h"
@interface ConfTest : XCTestCase

@end

@implementation ConfTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
     
    
}

- (void)testGetURLConServicioSinParametros{
    NSString *expUrl = @"https://api.mercadopago.com/v1/OOOP?public_key=444a9ef5%E2%80%908a6b%E2%80%90429f%E2%80%90abdf%E2%80%90587639155d88";
    NSURL *urlTest = [ConfHelper urlParaServicio:@"OOOP" ConParametros:nil];
    NSString *rulStr = urlTest.absoluteString;
    XCTAssertEqualObjects(expUrl, rulStr,@"EEEP");
    
}
- (void)testGetURLConServicioConParametros{
    NSString *expUrl = @"https://api.mercadopago.com/v1/OOOP?public_key=444a9ef5%E2%80%908a6b%E2%80%90429f%E2%80%90abdf%E2%80%90587639155d88&KEY=VALUE";
    NSURL *urlTest = [ConfHelper urlParaServicio:@"OOOP" ConParametros:@{@"KEY":@"VALUE"}];
    NSString *rulStr = urlTest.absoluteString;
    XCTAssertEqualObjects(expUrl, rulStr,@"EEEP");
    
}
- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}



- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
