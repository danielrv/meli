//
//  ServiceTest.m
//  MeLiDemoTests
//
//  Created by Daniel Rodriguez on 12/23/17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ServiceHelper.h"
#import "ConfHelper.h"
#import "MedioPago.h"

#define TestNeedsToWaitForBlock() __block BOOL blockFinished = NO
#define BlockFinished() blockFinished = YES
#define WaitForBlock() while (CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0, true) && !blockFinished)

@interface ServiceTest : XCTestCase

@end

@implementation ServiceTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}
- (void)testObtenerData{
    TestNeedsToWaitForBlock();
    NSURL *testURL = [ConfHelper urlParaServicio:@"payment_methods" ConParametros:nil];
    __block ServiceHelper *service =[[ServiceHelper alloc] init];
    NSLog(@"AAAAAA");
    [service obtenerArrayDe:[MedioPago class] ConUrl:testURL completado:^(NSArray *lista) {
        XCTAssert(![lista isMemberOfClass:NSArray.class], @"Respuesta no es Lista");
        BlockFinished();
    }];
   
    WaitForBlock();
    
}
- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}



- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
