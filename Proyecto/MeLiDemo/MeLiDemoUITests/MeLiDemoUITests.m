//
//  MeLiDemoUITests.m
//  MeLiDemoUITests
//
//  Created by Daniel Rodriguez on 12/23/17.
//  Copyright © 2017 Daniel Rodriguez. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface MeLiDemoUITests : XCTestCase

@end

@implementation MeLiDemoUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

/**
 Test para revisar el flujo introduciendo 5000 , Visa, Tarjeta Shopping y 12 cuotas.
 */
- (void)testFlujo {
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    
    [app.buttons[@"Empezar"] tap];
    
    XCUIElement *montoAPagarTextField = app.textFields[@"Monto a Pagar?"];
    [montoAPagarTextField tap];
    [montoAPagarTextField typeText:@"5000"];
    [app.buttons[@"SIGUIENTE"] tap];
    
    XCUIElementQuery *tablesQuery = app.tables;
    NSInteger cantidad = [[tablesQuery.element childrenMatchingType:XCUIElementTypeCell] count];
    XCTAssertEqual(cantidad, 21);
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"Visa"]/*[[".cells.staticTexts[@\"Visa\"]",".staticTexts[@\"Visa\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"Tarjeta Shopping"]/*[[".cells.staticTexts[@\"Tarjeta Shopping\"]",".staticTexts[@\"Tarjeta Shopping\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
    XCUIElementQuery *pickerQuery = app.pickerWheels;
    [pickerQuery[@"6 cuotas de $ 995,83 ($ 5.975,00)"] swipeUp];
    //[app/*@START_MENU_TOKEN@*/.pickerWheels[@"6 cuotas de $ 995,83 ($ 5.975,00)"]/*[[".pickers.pickerWheels[@\"6 cuotas de $ 995,83 ($ 5.975,00)\"]",".pickerWheels[@\"6 cuotas de $ 995,83 ($ 5.975,00)\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ swipeUp];
    [app.buttons[@"FINALIZAR"] tap];
    XCUIElement *lblMp = [app.staticTexts elementMatchingType:XCUIElementTypeAny identifier:@"Visa"];
    XCTAssert(lblMp != nil);
    [app.buttons[@"Cerrar"] tap];
   
    
}

@end
